(function ($, Drupal, drupalSettings) {
  "use strict";

  function checkOffsetmobile() {
    if (
      $(document).scrollTop() >
      $("#title-body").outerHeight(true) +
      $(".content-text-mobile").outerHeight(true) +
      120
    ) {
      $(".static").css({ position: "fixed" });
      $(".static").css("top", 0);
      $(".static").css("width", "98%");
    }

    if (
      $(document).scrollTop() <
      $("#title-body").outerHeight(true) +
      $(".content-text-mobile").outerHeight(true) +
      130
    ) {
      $(".static").css({ position: "", width: "" });
      $(".static").css("top", 0);
    }
  }

  $("#generic-pagespage select").on("change", function () {
    var top1 = $("header").outerHeight(true);
    var top2 = $("#title-body").outerHeight(true);

    var top3 = $(".hidden-md .content-text-mobile").outerHeight(true);
    var top4 = $(".static").outerHeight(true);
    if (
      $(document).scrollTop() >
      $("#title-body").outerHeight(true)
    ) {
      $("body,html").animate({
        scrollTop:
          $("#" + $(this).val()).position().top + top3 + top1 + top4 - 20
      });
    } else {
      $("body,html").animate({
        scrollTop:
          $("#" + $(this).val()).position().top + top3 + 30
      });
    }
  });

  var top1 = $("header").outerHeight(true);
  var top2 = $("#title-body").outerHeight(true);
  var heightlist = $(".padding-0").outerHeight(true);

  $("#page-wrap").css("height", $(window).height() - top1 - top2);
  // Select all links with hashes
  $('.onlyforgeneric a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function (event) {
      // On-page links
      if (
        location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
        location.hostname == this.hostname
      ) {
        // Figure out element to scroll to
        var target = $(this.hash);
        target = target.length
          ? target
          : $("[name=" + this.hash.slice(1) + "]");
        // Does a scroll target exist?
        if (target.length) {
          // Only prevent default if animation is actually gonna happen
          event.preventDefault();
          var top1 = $("header").outerHeight(true);
          var top2 = $("#title-body").outerHeight(true);
          $("html, body").animate(
            {
              scrollTop: target.offset().top
            },
            2000
          );
        }
      }
    });
  var a = 0;
  $(document).scroll(function () {
    checkOffset();
    checkOffsetmobile();
  });

  function checkOffset() {
    $(".generic-list:first-child").css({ "font-weight": "" });
    var top2 = $("#title-body").outerHeight(true);
    var top1 = $("header").outerHeight(true);
    $("#page-wrap").css("height", $(window).height() - top1 - 46);

    if (
      $('#title-body').offset().top - $(window).scrollTop() < -25
      // $(".breadcrumb").outerHeight(true)
    ) {

      $("#page-wrap").css({ position: "fixed" });
      $("#page-wrap").css({ top: 50, width: "" });
    }
    if (
      $('#title-body').offset().top - $(window).scrollTop() > -25
    ) {
      $("#page-wrap").css({ position: "" });
      $(".content-text").css({ "padding-top": "" });
      $("#page-wrap").css({ top: "", width: "" });
    }
    if (
      $(document).scrollTop() <
      top1 - $("#title-body").outerHeight(true)
    ) {
      $("#page-wrap").css(
        "height",
        $(window).height() - top1 - top2 - 46
      );
    }
    if ($('#page-wrap').length) {
      var a = $("#page-wrap");
      var div1 = $(".padding-0");
      var div1_top = a.offset().top;
      var div1_bottom = div1_top + a.height();
    }
  }

  $(window).on("load", function () {
    function formatDesign(item) {
      var selectionText = item.text.split(".");
      if (selectionText[1] != undefined)
        var $returnString =
          selectionText[0] + "</br>" + selectionText[1];
      else var $returnString = selectionText[0];
      return $returnString;
    }

    $("#selectBox").select2({
      placeholder: "Select something",
      minimumResultsForSearch: Infinity, //removes the search box
      formatResult: formatDesign,
      formatSelection: formatDesign
    });
    $('#selectBox0, #selectBox1, #selectBox2, #selectBox3').select2({
      placeholder: "Select something",
      formatResult: formatDesign,
      formatSelection: formatDesign
    });
  });

})(jQuery, Drupal, drupalSettings);